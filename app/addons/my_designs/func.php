<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;
use Tygh\Languages\Languages;
use Tygh\BlockManager\Block;
use Tygh\Tools\SecurityHelper;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

/**
 * Gets my_designs list by search params
 *
 * @param array  $params         my_design search params
 * @param string $lang_code      2 letters language code
 * @param int    $items_per_page Items per page
 *
 * @return array my_designs list and Search params
 */
function fn_get_my_designs($params = array(), $lang_code = CART_LANGUAGE, $items_per_page = 0)
{
    // Set default values to input params
    $default_params = array(
        'page' => 1,
        'items_per_page' => $items_per_page
    );

    $params = array_merge($default_params, $params);

    $sortings = array(
        'position' => '?:my_designs.position',
        'timestamp' => '?:my_designs.timestamp',
        'name' => '?:my_design_descriptions.my_design',
        'type' => '?:my_designs.type',
        'status' => '?:my_designs.status',
    );

    $condition = $limit = $join = '';

    if (!empty($params['limit'])) {
        $limit = db_quote(' LIMIT 0, ?i', $params['limit']);
    }

    $sorting = db_sort($params, $sortings, 'name', 'asc');

    $condition = (AREA == 'A') ? '' : " AND ?:my_designs.status = 'A' ";
    $condition .= fn_get_localizations_condition('?:my_designs.localization');
    $condition .= (AREA == 'A') ? '' : " AND (?:my_designs.type != 'G' OR ?:my_design_images.my_design_image_id IS NOT NULL) ";

    if (!empty($params['item_ids'])) {
        $condition .= db_quote(' AND ?:my_designs.my_design_id IN (?n)', explode(',', $params['item_ids']));
    }

    if (!empty($params['name'])) {
        $condition .= db_quote(' AND ?:my_design_descriptions.my_design LIKE ?l', '%' . trim($params['name']) . '%');
    }

    if (!empty($params['type'])) {
        $condition .= db_quote(' AND ?:my_designs.type = ?s', $params['type']);
    }

    if (!empty($params['status'])) {
        $condition .= db_quote(' AND ?:my_designs.status = ?s', $params['status']);
    }

    if (!empty($params['period']) && $params['period'] != 'A') {
        list($params['time_from'], $params['time_to']) = fn_create_periods($params);
        $condition .= db_quote(' AND (?:my_designs.timestamp >= ?i AND ?:my_designs.timestamp <= ?i)', $params['time_from'], $params['time_to']);
    }

    $fields = array (
        '?:my_designs.my_design_id',
        '?:my_designs.type',
        '?:my_designs.target',
        '?:my_designs.status',
        '?:my_designs.position',
        '?:my_design_descriptions.my_design',
        '?:my_design_descriptions.description',
        '?:my_design_descriptions.url',
        '?:my_design_images.my_design_image_id',
    );

    if (fn_allowed_for('ULTIMATE')) {
        $fields[] = '?:my_designs.company_id';
    }

    /**
     * This hook allows you to change parameters of the my_design selection before making an SQL query.
     *
     * @param array        $params    The parameters of the user's query (limit, period, item_ids, etc)
     * @param string       $condition The conditions of the selection
     * @param string       $sorting   Sorting (ask, desc)
     * @param string       $limit     The LIMIT of the returned rows
     * @param string       $lang_code Language code
     * @param array        $fields    Selected fields
     */
    fn_set_hook('get_my_designs', $params, $condition, $sorting, $limit, $lang_code, $fields);

    $join .= db_quote(' LEFT JOIN ?:my_design_descriptions ON ?:my_design_descriptions.my_design_id = ?:my_designs.my_design_id AND ?:my_design_descriptions.lang_code = ?s', $lang_code);
    $join .= db_quote(' LEFT JOIN ?:my_design_images ON ?:my_design_images.my_design_id = ?:my_designs.my_design_id AND ?:my_design_images.lang_code = ?s', $lang_code);

    if (!empty($params['items_per_page'])) {
        $params['total_items'] = db_get_field("SELECT COUNT(*) FROM ?:my_designs $join WHERE 1 $condition");
        $limit = db_paginate($params['page'], $params['items_per_page'], $params['total_items']);
    }

    $my_designs = db_get_hash_array(
        "SELECT ?p FROM ?:my_designs " .
        $join .
        "WHERE 1 ?p ?p ?p",
        'my_design_id', implode(', ', $fields), $condition, $sorting, $limit
    );

    if (!empty($params['item_ids'])) {
        $my_designs = fn_sort_by_ids($my_designs, explode(',', $params['item_ids']), 'my_design_id');
    }

    $my_design_image_ids = fn_array_column($my_designs, 'my_design_image_id');
    $images = fn_get_image_pairs($my_design_image_ids, 'promo', 'M', true, false, $lang_code);

    foreach ($my_designs as $my_design_id => $my_design) {
        $my_designs[$my_design_id]['main_pair'] = !empty($images[$my_design['my_design_image_id']]) ? reset($images[$my_design['my_design_image_id']]) : array();
    }

    fn_set_hook('get_my_designs_post', $my_designs, $params);

    return array($my_designs, $params);
}

//
// Get specific my_design data
//
function fn_get_my_design_data($my_design_id, $lang_code = CART_LANGUAGE)
{
    // Unset all SQL variables
    $fields = $joins = array();
    $condition = '';

    $fields = array (
        '?:my_designs.my_design_id',
        '?:my_designs.status',
        '?:my_design_descriptions.my_design',
        '?:my_designs.type',
        '?:my_designs.target',
        '?:my_designs.localization',
        '?:my_designs.timestamp',
        '?:my_designs.position',
        '?:my_design_descriptions.description',
        '?:my_design_descriptions.url',
        '?:my_design_images.my_design_image_id',
    );

    if (fn_allowed_for('ULTIMATE')) {
        $fields[] = '?:my_designs.company_id as company_id';
    }

    $joins[] = db_quote("LEFT JOIN ?:my_design_descriptions ON ?:my_design_descriptions.my_design_id = ?:my_designs.my_design_id AND ?:my_design_descriptions.lang_code = ?s", $lang_code);
    $joins[] = db_quote("LEFT JOIN ?:my_design_images ON ?:my_design_images.my_design_id = ?:my_designs.my_design_id AND ?:my_design_images.lang_code = ?s", $lang_code);

    $condition = db_quote("WHERE ?:my_designs.my_design_id = ?i", $my_design_id);
    $condition .= (AREA == 'A') ? '' : " AND ?:my_designs.status IN ('A', 'H') ";

    /**
     * Prepare params for my_design data SQL query
     *
     * @param int   $my_design_id my_design ID
     * @param str   $lang_code Language code
     * @param array $fields    Fields list
     * @param array $joins     Joins list
     * @param str   $condition Conditions query
     */
    fn_set_hook('get_my_design_data', $my_design_id, $lang_code, $fields, $joins, $condition);

    $my_design = db_get_row("SELECT " . implode(", ", $fields) . " FROM ?:my_designs " . implode(" ", $joins) ." $condition");

    if (!empty($my_design)) {
        $my_design['main_pair'] = fn_get_image_pairs($my_design['my_design_image_id'], 'promo', 'M', true, false, $lang_code);
    }

    /**
     * Post processing of my_design data
     *
     * @param int   $my_design_id my_design ID
     * @param str   $lang_code Language code
     * @param array $my_design    my_design data
     */
    fn_set_hook('get_my_design_data_post', $my_design_id, $lang_code, $my_design);

    return $my_design;
}

/**
 * Hook for deleting store my_designs
 *
 * @param int $company_id Company id
 */
function fn_my_designs_delete_company(&$company_id)
{
    if (fn_allowed_for('ULTIMATE')) {
        $bannser_ids = db_get_fields("SELECT my_design_id FROM ?:my_designs WHERE company_id = ?i", $company_id);

        foreach ($bannser_ids as $my_design_id) {
            fn_delete_my_design_by_id($my_design_id);
        }
    }
}

/**
 * Deletes my_design and all related data
 *
 * @param int $my_design_id my_design identificator
 */
function fn_delete_my_design_by_id($my_design_id)
{
    if (!empty($my_design_id) && fn_check_company_id('my_designs', 'my_design_id', $my_design_id)) {
        db_query("DELETE FROM ?:my_designs WHERE my_design_id = ?i", $my_design_id);
        db_query("DELETE FROM ?:my_design_descriptions WHERE my_design_id = ?i", $my_design_id);

        fn_set_hook('delete_my_designs', $my_design_id);

        Block::instance()->removeDynamicObjectData('my_designs', $my_design_id);

        $my_design_images_ids = db_get_fields("SELECT my_design_image_id FROM ?:my_design_images WHERE my_design_id = ?i", $my_design_id);

        foreach ($my_design_images_ids as $my_design_image_id) {
            fn_delete_image_pairs($my_design_image_id, 'promo');
        }

        db_query("DELETE FROM ?:my_design_images WHERE my_design_id = ?i", $my_design_id);
    }
}

/**
 * Checks of request for need to update the my_design image.
 *
 * @return bool
 */
function fn_my_designs_need_image_update()
{
    if (!empty($_REQUEST['file_my_designs_main_image_icon']) && is_array($_REQUEST['file_my_designs_main_image_icon'])) {
        $image_my_design = reset($_REQUEST['file_my_designs_main_image_icon']);

        if ($image_my_design == 'my_designs_main') {
            return false;
        }
    }

    return true;
}

function fn_my_designs_update_my_design($data, $my_design_id, $lang_code = DESCR_SL)
{
    SecurityHelper::sanitizeObjectData('my_design', $data);

    if (isset($data['timestamp'])) {
        $data['timestamp'] = fn_parse_date($data['timestamp']);
    }

    $data['localization'] = empty($data['localization']) ? '' : fn_implode_localizations($data['localization']);

    if (!empty($my_design_id)) {
        db_query("UPDATE ?:my_designs SET ?u WHERE my_design_id = ?i", $data, $my_design_id);
        db_query("UPDATE ?:my_design_descriptions SET ?u WHERE my_design_id = ?i AND lang_code = ?s", $data, $my_design_id, $lang_code);

        $my_design_image_id = fn_get_my_design_image_id($my_design_id, $lang_code);
        $my_design_image_exist = !empty($my_design_image_id);
        $my_design_is_multilang = Registry::get('addons.my_designs.my_design_multilang') == 'Y';
        $image_is_update = fn_my_designs_need_image_update();

        if ($my_design_is_multilang) {
            if ($my_design_image_exist && $image_is_update) {
                fn_delete_image_pairs($my_design_image_id, 'promo');
                db_query("DELETE FROM ?:my_design_images WHERE my_design_id = ?i AND lang_code = ?s", $my_design_id, $lang_code);
                $my_design_image_exist = false;
            }
        } else {
            if (isset($data['url'])) {
                db_query("UPDATE ?:my_design_descriptions SET url = ?s WHERE my_design_id = ?i", $data['url'], $my_design_id);
            }
        }

        if ($image_is_update && !$my_design_image_exist) {
            $my_design_image_id = db_query("INSERT INTO ?:my_design_images (my_design_id, lang_code) VALUE(?i, ?s)", $my_design_id, $lang_code);
        }
        $pair_data = fn_attach_image_pairs('my_designs_main', 'promo', $my_design_image_id, $lang_code);

        if (!$my_design_is_multilang && !$my_design_image_exist) {
            fn_my_designs_image_all_links($my_design_id, $pair_data, $lang_code);
        }

    } else {
        $my_design_id = $data['my_design_id'] = db_query("REPLACE INTO ?:my_designs ?e", $data);

        foreach (Languages::getAll() as $data['lang_code'] => $v) {
            db_query("REPLACE INTO ?:my_design_descriptions ?e", $data);
        }

        if (fn_my_designs_need_image_update()) {
            $my_design_image_id = db_get_next_auto_increment_id('my_design_images');
            $pair_data = fn_attach_image_pairs('my_designs_main', 'promo', $my_design_image_id, $lang_code);
            if (!empty($pair_data)) {
                $data_my_design_image = array(
                    'my_design_image_id' => $my_design_image_id,
                    'my_design_id'       => $my_design_id,
                    'lang_code'       => $lang_code
                );

                db_query("INSERT INTO ?:my_design_images ?e", $data_my_design_image);
                fn_my_designs_image_all_links($my_design_id, $pair_data, $lang_code);
            }
        }
    }

    return $my_design_id;
}

function fn_my_designs_image_all_links($my_design_id, $pair_data, $main_lang_code = DESCR_SL)
{
    if (!empty($pair_data)) {
        $pair_id = reset($pair_data);

        $lang_codes = Languages::getAll();
        unset($lang_codes[$main_lang_code]);

        foreach ($lang_codes as $lang_code => $lang_data) {
            $_my_design_image_id = db_query("INSERT INTO ?:my_design_images (my_design_id, lang_code) VALUE(?i, ?s)", $my_design_id, $lang_code);
            fn_add_image_link($_my_design_image_id, $pair_id);
        }
    }
}

function fn_get_my_design_image_id($my_design_id, $lang_code = DESCR_SL)
{
    return db_get_field("SELECT my_design_image_id FROM ?:my_design_images WHERE my_design_id = ?i AND lang_code = ?s", $my_design_id, $lang_code);
}

//
// Get my_design name
//
function fn_get_my_design_name($my_design_id, $lang_code = CART_LANGUAGE)
{
    if (!empty($my_design_id)) {
        return db_get_field("SELECT my_design FROM ?:my_design_descriptions WHERE my_design_id = ?i AND lang_code = ?s", $my_design_id, $lang_code);
    }

    return false;
}

function fn_my_designs_delete_image_pre($image_id, $pair_id, $object_type)
{
    if ($object_type == 'promo') {
        $my_design_data = db_get_row("SELECT my_design_id, my_design_image_id FROM ?:my_design_images INNER JOIN ?:images_links ON object_id = my_design_image_id WHERE pair_id = ?i", $pair_id);

        if (Registry::get('addons.my_designs.my_design_multilang') == 'Y') {

            if (!empty($my_design_data['my_design_image_id'])) {
                $lang_code = db_get_field("SELECT lang_code FROM ?:my_design_images WHERE my_design_image_id = ?i", $my_design_data['my_design_image_id']);

                db_query("DELETE FROM ?:common_descriptions WHERE object_id = ?i AND object_holder = 'images' AND lang_code = ?s", $image_id, $lang_code);
                db_query("DELETE FROM ?:my_design_images WHERE my_design_image_id = ?i", $my_design_data['my_design_image_id']);
            }

        } else {
            $my_design_image_ids = db_get_fields("SELECT object_id FROM ?:images_links WHERE image_id = ?i AND object_type = 'promo'", $image_id);

            if (!empty($my_design_image_ids)) {
                db_query("DELETE FROM ?:my_design_images WHERE my_design_image_id IN (?n)", $my_design_image_ids);
                db_query("DELETE FROM ?:images_links WHERE object_id IN (?n)", $my_design_image_ids);
            }
        }
    }
}

function fn_my_designs_clone($my_designs, $lang_code)
{
    foreach ($my_designs as $my_design) {
        if (empty($my_design['main_pair']['pair_id'])) {
            continue;
        }

        $data_my_design_image = array(
            'my_design_id' => $my_design['my_design_id'],
            'lang_code' => $lang_code
        );
        $my_design_image_id = db_query("REPLACE INTO ?:my_design_images ?e", $data_my_design_image);
        fn_add_image_link($my_design_image_id, $my_design['main_pair']['pair_id']);
    }
}

function fn_my_designs_update_language_post($language_data, $lang_id, $action)
{
    if ($action == 'add') {
        list($my_designs) = fn_get_my_designs(array(), DEFAULT_LANGUAGE);
        fn_my_designs_clone($my_designs, $language_data['lang_code']);
    }
}

function fn_my_designs_delete_languages_post($lang_ids, $lang_codes, $deleted_lang_codes)
{
    foreach ($deleted_lang_codes as $lang_code) {
        list($my_designs) = fn_get_my_designs(array(), $lang_code);

        foreach ($my_designs as $my_design) {
            if (empty($my_design['main_pair']['pair_id'])) {
                continue;
            }
            fn_delete_image($my_design['main_pair']['image_id'], $my_design['main_pair']['pair_id'], 'promo');
        }
    }
}

function fn_my_designs_install()
{
    $my_designs = db_get_hash_multi_array("SELECT ?:my_designs.my_design_id, ?:my_design_images.my_design_image_id, ?:my_design_images.lang_code FROM ?:my_designs LEFT JOIN ?:my_design_images ON ?:my_design_images.my_design_id = ?:my_designs.my_design_id", array('lang_code', 'my_design_id'));

    $langs = array_keys(Languages::getAll());
    $need_clone_langs = array_diff($langs, array_keys($my_designs));

    if (!empty($need_clone_langs)) {
        $clone_lang = DEFAULT_LANGUAGE;

        if (in_array(DEFAULT_LANGUAGE, $need_clone_langs)) {
            $clone_lang = 'en';
        }

        foreach ($my_designs[$clone_lang] as $my_design_id => &$my_design) {
            $my_design['main_pair'] = fn_get_image_pairs($my_design['my_design_image_id'], 'promo', 'M', true, false, $clone_lang);
        }

        foreach ($need_clone_langs as $need_clone_lang) {
            fn_my_designs_clone($my_designs[$clone_lang], $need_clone_lang);
        }
    }

    foreach ($my_designs['en'] as $my_design_id => &$my_design) {
        $my_design['main_pair'] = fn_get_image_pairs($my_design['my_design_image_id'], 'promo', 'M', true, false, 'en');
    }

    if (!in_array('en', $langs)) {
        $my_design_images_ids = db_get_fields("SELECT my_design_image_id FROM ?:my_design_images WHERE lang_code = ?s", 'en');
        foreach ($my_design_images_ids as $my_design_image_id) {
            fn_delete_image_pairs($my_design_image_id, 'promo');
        }

        if (!empty($my_design_images_ids)) {
            db_query("DELETE FROM ?:my_design_images WHERE my_design_image_id IN (?n)", $my_design_images_ids);
        }
    }

    return true;
}

if (!fn_allowed_for('ULTIMATE:FREE')) {
    function fn_my_designs_localization_objects(&$_tables)
    {
        $_tables[] = 'my_designs';
    }
}

if (fn_allowed_for('ULTIMATE')) {
    function fn_my_designs_ult_check_store_permission($params, &$object_type, &$object_name, &$table, &$key, &$key_id)
    {
        if (Registry::get('runtime.controller') == 'my_designs' && !empty($params['my_design_id'])) {
            $key = 'my_design_id';
            $key_id = $params[$key];
            $table = 'my_designs';
            $object_name = fn_get_my_design_name($key_id, DESCR_SL);
            $object_type = __('my_design');
        }
    }
}
