<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD']	== 'POST') {

    fn_trusted_vars('my_designs', 'my_design_data');
    $suffix = '';

    //
    // Delete my_designs
    //
    if ($mode == 'm_delete') {
        foreach ($_REQUEST['my_design_ids'] as $v) {
            fn_delete_my_design_by_id($v);
        }

        $suffix = '.manage';
    }

    //
    // Add/edit my_designs
    //
    if ($mode == 'update') {
        $my_design_id = fn_my_designs_update_my_design($_REQUEST['my_design_data'], $_REQUEST['my_design_id'], DESCR_SL);

        $suffix = ".update?my_design_id=$my_design_id";
    }

    if ($mode == 'delete') {
        if (!empty($_REQUEST['my_design_id'])) {
            fn_delete_my_design_by_id($_REQUEST['my_design_id']);
        }

        $suffix = '.manage';
    }

    return array(CONTROLLER_STATUS_OK, 'my_designs' . $suffix);
}

if ($mode == 'update') {
    $my_design = fn_get_my_design_data($_REQUEST['my_design_id'], DESCR_SL);

    if (empty($my_design)) {
        return array(CONTROLLER_STATUS_NO_PAGE);
    }

    Registry::set('navigation.tabs', array (
        'general' => array (
            'title' => __('general'),
            'js' => true
        ),
    ));

    Tygh::$app['view']->assign('my_design', $my_design);

} elseif ($mode == 'manage' || $mode == 'picker') {

    list($my_designs, $params) = fn_get_my_designs($_REQUEST, DESCR_SL, Registry::get('settings.Appearance.admin_elements_per_page'));

    Tygh::$app['view']->assign('my_designs', $my_designs);
    Tygh::$app['view']->assign('search', $params);
}

//
// my_designs picker
//
if ($mode == 'picker') {
    Tygh::$app['view']->display('addons/my_designs/pickers/my_designs/picker_contents.tpl');
    exit;
}
