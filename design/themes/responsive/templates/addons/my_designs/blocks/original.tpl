{** block-description:original **}
{foreach from=$items item="my_design" key="key"}
    {if $my_design.type == "G" && $my_design.main_pair.image_id}
    <div class="ty-my_design__image-wrapper">
        {if $my_design.url != ""}<a href="{$my_design.url|fn_url}" {if $my_design.target == "B"}target="_blank"{/if}>{/if}
        {include file="common/image.tpl" images=$my_design.main_pair image_auto_size=true}
        {if $my_design.url != ""}</a>{/if}
    </div>
    {else}
        <div class="ty-wysiwyg-content">
            {$my_design.description nofilter}
        </div>
    {/if}
{/foreach}
