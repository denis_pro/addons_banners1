{** block-description:carousel **}

{if $items}
    <div id="my_design_slider_{$block.snapping_id}" class="my_designs owl-carousel">
        {foreach from=$items item="my_design" key="key"}
            <div class="ty-my_design__image-item">
                {if $my_design.type == "G" && $my_design.main_pair.image_id}
                    {if $my_design.url != ""}<a class="my_design__link" href="{$my_design.url|fn_url}" {if $my_design.target == "B"}target="_blank"{/if}>{/if}
                        {include file="common/image.tpl" images=$my_design.main_pair class="ty-my_design__image" }
                    {if $my_design.url != ""}</a>{/if}
                {else}
                    <div class="ty-wysiwyg-content">
                        {$my_design.description nofilter}
                    </div>
                {/if}
            </div>
        {/foreach}
    </div>
{/if}

<script type="text/javascript">
(function(_, $) {
    $.ceEvent('on', 'ce.commoninit', function(context) {
        var slider = context.find('#my_design_slider_{$block.snapping_id}');
        if (slider.length) {
            slider.owlCarousel({
                direction: '{$language_direction}',
                items: 1,
                singleItem : true,
                slideSpeed: {$block.properties.speed|default:400},
                autoPlay: '{$block.properties.delay * 1000|default:false}',
                stopOnHover: true,
                {if $block.properties.navigation == "N"}
                    pagination: false
                {/if}
                {if $block.properties.navigation == "D"}
                    pagination: true
                {/if}
                {if $block.properties.navigation == "P"}
                    pagination: true,
                    paginationNumbers: true
                {/if}
                {if $block.properties.navigation == "A"}
                    pagination: false,
                    navigation: true,
                    navigationText: ['{__("prev_page")}', '{__("next")}']
                {/if}
            });
        }
    });
}(Tygh, Tygh.$));
</script>
